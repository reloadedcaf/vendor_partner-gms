###############################################################################
# GmsCoreGo
LOCAL_PATH := $(my-dir)

my_archs := arm arm64
my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))

include $(CLEAR_VARS)
LOCAL_MODULE := GmsCoreGo
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
# Exclude the AOSP NetworkRecommendation/StatementService apps as gcore has its own implementation.
LOCAL_OVERRIDES_PACKAGES := NetworkRecommendation StatementService
LOCAL_OPTIONAL_USES_LIBRARIES := org.apache.http.legacy android.ext.adservices com.android.media.remotedisplay com.android.location.provider samsung.uwb androidx.window.extensions androidx.window.sidecar com.google.android.wearable
#LOCAL_REQUIRED_MODULES :=
LOCAL_MODULE_TARGET_ARCH := $(my_src_arch)
ifeq ($(my_src_arch),arm64)
  ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),false)
    LOCAL_MULTILIB := 64
  else
    LOCAL_MULTILIB := both
  endif
endif
include $(BUILD_PREBUILT)
