###############################################################################
# GMS home folder location
# Note: we cannot use $(my-dir) in this makefile
ANDROID_PARTNER_GMS_HOME := vendor/partner_gms

# Android Build system uncompresses DEX files (classes*.dex) in the privileged
# apps by default, which breaks APK v2/v3 signature. Because some privileged
# GMS apps are still shipped with compressed DEX, we need to disable
# uncompression feature to make them work correctly.
DONT_UNCOMPRESS_PRIV_APPS_DEXS := true

PRODUCT_SOONG_NAMESPACES += vendor/partner_gms/etc/sysconfig/gms_go vendor/partner_gms/etc/preferred-apps/gms_go

# GMS mandatory core packages
PRODUCT_PACKAGES := \
    AndroidPlatformServicesGo \
    ConfigUpdater \
    FamilyLinkParentalControls \
    GoogleCalendarSyncAdapter \
    GoogleExtShared \
    GoogleFeedback \
    GoogleLocationHistory \
    GoogleOneTimeInitializer \
    GooglePackageInstaller \
    GooglePartnerSetup \
    GooglePrintRecommendationService \
    GoogleRestore \
    GoogleServicesFramework \
    PrivateComputeServices \
    SpeechServicesByGoogle \
    GmsCoreGo \
    Phonesky \
    SetupWizard \
    WebViewGoogle \
    Wellbeing

# GMS RRO packages
PRODUCT_PACKAGES += \
    GmsConfigOverlayCommon \
    GmsConfigOverlayGo \
    GmsConfigOverlaySearchGo \
    GmsConfigOverlayGeotz

# GMS common configuration files
PRODUCT_PACKAGES += \
    play_store_fsi_cert \
    gms_fsverity_cert \
    default_permissions_allowlist_google \
    privapp_permissions_google_system \
    privapp_permissions_google_product \
    privapp_permissions_google_system_ext \
    split_permissions_google \
    preferred_apps_google \
    sysconfig_google \
    google_hiddenapi_package_allowlist \
    android_go-initial-package-stopped-states.xml \
    google-initial-package-stopped-states.xml

# Overlay for GMS devices: default backup transport in SettingsProvider
PRODUCT_PACKAGE_OVERLAYS += $(ANDROID_PARTNER_GMS_HOME)/overlay/gms_overlay

# GMS mandatory application packages
PRODUCT_PACKAGES += \
    AssistantGo \
    Chrome \
    Drive \
    GalleryGo \
    Gmail2 \
    GoogleSearchGo \
    LatinImeGoogleGo \
    Maps \
    MeetGo \
    YouTube \
    YTMusic \
    Videos

# GMS comms suite
$(call inherit-product, $(ANDROID_PARTNER_GMS_HOME)/products/google_go_comms_suite.mk)

# Personal Safety
$(call inherit-product, $(ANDROID_PARTNER_GMS_HOME)/products/personal_safety.mk)

# GMS optional application packages
PRODUCT_PACKAGES += \
    CalculatorGoogle \
    CalendarGoogle \
    DeskClockGoogle \
    FilesGoogle \
    SwitchAccess \
    TagGoogle \
    talkback

# GMS sample application packages
PRODUCT_PACKAGES += \
    GmsSampleIntegrationGo_2GB \
    Launcher3QuickStepGo

include $(ANDROID_PARTNER_GMS_HOME)/products/gms_package_version.mk

PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.rotation_locked=true \
    setupwizard.theme=glif_v3_light \
    ro.opa.eligible_device=true \
    ro.com.google.gmsversion=$(GMS_PACKAGE_VERSION_ID).go
