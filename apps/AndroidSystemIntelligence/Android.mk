###############################################################################
# AndroidSystemIntelligence
LOCAL_PATH := $(my-dir)

my_archs := arm arm64
my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))

# Set the type of ASI apk to preload (Features or Infrastructure)
GMS_ASI_APK_PRELOAD_TYPE := Features

include $(CLEAR_VARS)
LOCAL_MODULE := AndroidSystemIntelligence
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRIVILEGED_MODULE := true
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(GMS_ASI_APK_PRELOAD_TYPE)_$(my_src_arch).apk
#LOCAL_OVERRIDES_PACKAGES :=
LOCAL_OPTIONAL_USES_LIBRARIES := androidx.window.extensions androidx.window.sidecar
ifeq ($(GMS_ASI_APK_PRELOAD_TYPE),Features)
LOCAL_REQUIRED_MODULES := GmsConfigOverlayASI sysconfig_asi_features PrivateComputeServices
else ifeq ($(GMS_ASI_APK_PRELOAD_TYPE),Infrastructure)
LOCAL_REQUIRED_MODULES := GmsConfigOverlayASI sysconfig_asi_infrastructure PrivateComputeServices
endif
LOCAL_MODULE_TARGET_ARCH := $(my_src_arch)
include $(BUILD_PREBUILT)

# Clear temporary variable
GMS_ASI_APK_PRELOAD_TYPE :=
