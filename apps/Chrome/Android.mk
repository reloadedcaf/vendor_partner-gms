###############################################################################
# Chrome
LOCAL_PATH := $(my-dir)

my_archs := arm arm64
my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),true)
include $(CLEAR_VARS)
LOCAL_MODULE := Chrome
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
LOCAL_OVERRIDES_PACKAGES := Browser Browser2
LOCAL_REQUIRED_MODULES := TrichromeLibrary BookmarkProvider PartnerBookmarksProvider
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)
endif

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),true)
include $(CLEAR_VARS)
LOCAL_MODULE := TrichromeLibrary
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
#LOCAL_OVERRIDES_PACKAGES :=
#LOCAL_REQUIRED_MODULES :=
LOCAL_MULTILIB := both
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)
endif

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),false)
include $(CLEAR_VARS)
LOCAL_MODULE := Chrome64
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
LOCAL_OVERRIDES_PACKAGES := Browser Browser2 Chrome
LOCAL_REQUIRED_MODULES := TrichromeLibrary64 BookmarkProvider PartnerBookmarksProvider
LOCAL_MULTILIB := 64
include $(BUILD_PREBUILT)
endif

ifeq ($(TARGET_SUPPORTS_32_BIT_APPS),false)
include $(CLEAR_VARS)
LOCAL_MODULE := TrichromeLibrary64
LOCAL_MODULE_OWNER := google
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := $(LOCAL_MODULE)_$(my_src_arch).apk
LOCAL_OVERRIDES_PACKAGES := TrichromeLibrary
#LOCAL_REQUIRED_MODULES :=
LOCAL_MULTILIB := 64
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)
endif
